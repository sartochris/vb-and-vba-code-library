VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProgressControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Class to control the progress data of any calling process
'
' @author   Christoph Juengling
'
Option Explicit

Public Event displaytext(ByVal displaytext As String)
Public Event extendeddisplaytext(ByVal extendeddisplaytext As String)
Public Event maxValue(ByVal maxValue As Long)
Public Event step(ByVal step As Long)
Public Event hide()

Private m_strDisplaytext As String
Private m_strExtendedDisplaytext As String
Private m_lngmaxValue As Long
Public repeat As Boolean

Private stepCount As Long

Private Sub Class_Initialize()

reset

End Sub

Private Sub reset()

repeat = False
stepCount = 0
displaytext = ""
extendeddisplaytext = ""
m_lngmaxValue = 0

End Sub

Public Property Let displaytext(ByVal strDisplaytext As String)

m_strDisplaytext = Trim(strDisplaytext)
RaiseEvent displaytext(m_strDisplaytext)

End Property

Public Property Get displaytext() As String

displaytext = m_strDisplaytext

End Property

Public Property Let extendeddisplaytext(ByVal strExtendeddisplaytext As String)

m_strExtendedDisplaytext = Trim(strExtendeddisplaytext)
RaiseEvent extendeddisplaytext(m_strExtendedDisplaytext)

End Property

Public Property Get extendeddisplaytext() As String

extendeddisplaytext = m_strExtendedDisplaytext

End Property

Public Property Get maxValue() As Long

maxValue = m_lngmaxValue

End Property

Public Sub addsteps(ByVal steps As Long)

If steps > 0 Then
    m_lngmaxValue = m_lngmaxValue + steps
    If m_lngmaxValue > 0 Then RaiseEvent maxValue(m_lngmaxValue)
    
    #If DEBUG_MODE Then
        Debug.Print typeName(Me) & ":"
        Debug.Print "   Displaytext:  "; m_strDisplaytext
        Debug.Print "   Extended:     "; m_strExtendedDisplaytext
        Debug.Print "   Add steps:    "; steps
        Debug.Print "   Max value:    "; m_lngmaxValue
        Debug.Print "   Current step: "; stepCount
    #End If
End If

End Sub

Public Sub hide()

#If DEBUG_MODE Then
    Debug.Print typeName(Me) & ":"
    Debug.Print "   Displaytext:  "; m_strDisplaytext
    Debug.Print "   Extended:     "; m_strExtendedDisplaytext
    Debug.Print "   Max value:    "; m_lngmaxValue
    Debug.Print "   Last step:    "; stepCount
#End If

reset
RaiseEvent hide

End Sub

Public Sub step()

stepCount = stepCount + 1

' If step counter exceeds max value
If stepCount > m_lngmaxValue Then
    If repeat Then
        stepCount = 0
    Else
        ' Increase max value
        m_lngmaxValue = stepCount
        RaiseEvent maxValue(m_lngmaxValue)
    End If
End If

If isInUse() Then RaiseEvent step(stepCount)

#If DEBUG_MODE Then
    Debug.Print "   Current step: "; stepCount
#End If

End Sub

Public Property Get value() As Long

value = stepCount

End Property

''
' Class is supposed to be "in use", if at least one display text is not empty and maxValue is > 0
'
Public Property Get isInUse() As Boolean

isInUse = m_lngmaxValue > 0 And (Len(m_strDisplaytext) > 0 Or Len(m_strExtendedDisplaytext) > 0)

End Property

