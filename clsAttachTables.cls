Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'=======================================================================================
' Module  : clsAttachTables (Klassenmodul)
' Purpose : Manage actions for re-attaching linked tables
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
' Throws  : Database errors
' Raises  : Starting, NextTable, Aborted, Finished
' Uses    : (ref) Microsoft ADO Ext. 2.8 for DDL and Security (ADOX)
'           (ref) Microsoft ActiveX Data Objects 2.8 Library (ADODB)
'           (mod) ErrorDefinitions
'=======================================================================================

Option Compare Database
Option Explicit

Private Const TABLE_NAME = "EingebundeneTabellen"

Private cat As ADOX.Catalog

' Public events
Public Event Starting(NumberOfTables As Long)
Public Event NextTable(TableName As String)
Public Event Aborted(Message As String)
Public Event Finished(backendPath As String)

' Public properties without any checks
Public backendPath As String
Public checkOnly As Boolean

'=======================================================================================
' Purpose : (Re-)attach planned tables
' Throws  : ERR_ATTACHING_TABLES, all other errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
'=======================================================================================

Public Sub attach()

Const FUNCTION_NAME = "attach"

Dim fso As Scripting.FileSystemObject
Dim rs As ADODB.Recordset
Dim tbl As ADOX.Table
Dim found As Boolean
Dim es As tSavedError

'----------------------------

On Error GoTo Catch

If Trim(backendPath) = "" Then RaiseProjectError ERR_ATTACHING_TABLES, , "Missing Backend Path!"

RaiseEvent Starting(0)

' First check if backend path really exists
Set fso = New Scripting.FileSystemObject
If Not fso.FileExists(backendPath) Then
    RaiseProjectError ERR_ATTACHING_TABLES, , , "Backend file not found: " & backendPath
End If

' Open Control table
Set rs = New ADODB.Recordset
With rs
    .ActiveConnection = CurrentProject.Connection
    .CursorLocation = adUseClient
    .CursorType = adOpenDynamic
    .LockType = adLockPessimistic
    .source = TABLE_NAME
    .Open
    RaiseEvent Starting(.RecordCount)
End With

' Run through the planned tables
Do Until rs.EOF
    found = False
    For Each tbl In cat.Tables
        If StrComp(tbl.name, rs!Tabellenname, vbTextCompare) = 0 Then
            ' Table is already attached
            If StrComp(tbl.Type, "LINK", vbTextCompare) <> 0 Then
                ' Table exists, but it is a local table
                RaiseProjectError ERR_ATTACHING_TABLES, "", "Attached table """ & rs!Tabellenname & """ is stored locally."
            End If
            
            If tbl.Properties("Jet OLEDB:Link Datasource").value <> backendPath Then
                If checkOnly Then
                    RaiseProjectError ERR_ATTACHING_TABLES, "", "Attached table """ & rs!Tabellenname & """ is not linked correctly."
                Else
                    tbl.Properties("Jet OLEDB:Link Datasource").value = backendPath
                End If
            End If
            
            found = True
            Exit For
        End If
    Next tbl
        
    ' Create attached table
    If Not found Then
        'Err.Raise ERR_ATTACHING_TABLES, , "Table not found and cannot be attached: " & rs!Tabellenname
        attachNew rs!Tabellenname
    End If
    
    RaiseEvent NextTable(rs!Tabellenname)

    rs.MoveNext
    
    DoEvents
Loop

RaiseEvent Finished(backendPath)

'----------------------------
Final:
Set cat = Nothing
Set fso = Nothing

On Error GoTo 0
RaiseSavedError es

Exit Sub

'----------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.Number
    Case -2147467259 ' Sie haben nicht die n�tigen Berechtigungen, um das Objekt zu bearbeiten.
        RaiseEvent Aborted(Err.Description)

    Case Else
        Debug.Print Err.Number, Err.source, Err.Description
End Select

Resume Final
Resume

End Sub



Private Sub Class_Initialize()

backendPath = ""
checkOnly = False

' Catalog of current database
Set cat = New ADOX.Catalog
cat.ActiveConnection = CurrentProject.Connection

End Sub



'=======================================================================================
' Purpose : Attach new table
' Params  : TableName = name of the table in this database (frontend)
'           BackendTableName = name of this table in backend database
' Throws  : all errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
'=======================================================================================

Private Sub attachNew(TableName As String, Optional backendTableName As String = "")

Const FUNCTION_NAME = "attachNew"

Dim tbl As ADOX.Table
Dim es As tSavedError

'---------------------------

On Error GoTo Catch

' Create the new Table in the current catalog
Set tbl = New ADOX.Table
With tbl
    .name = TableName
    Set .ParentCatalog = cat

    ' Set the properties to create the link
    .Properties("Jet OLEDB:Link Provider String") = "MS Access"
    .Properties("Jet OLEDB:Link Datasource") = backendPath
    .Properties("Jet OLEDB:Remote Table Name") = IIf(backendTableName <> "", backendTableName, TableName)
    .Properties("Jet OLEDB:Create Link") = True
End With

' Append the table to the Tables collection
cat.Tables.append tbl

'---------------------------
Final:
On Error Resume Next
Set tbl = Nothing

On Error GoTo 0
RaiseSavedError es
Exit Sub

'---------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Resume Final
Resume

End Sub

Private Sub Class_Terminate()

Set cat = Nothing

End Sub



'=======================================================================================
' Purpose : Drop all linked tables
' Throws  : All encountered errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
' Remarks : Dropping all tables decreases the size of the MDB file.
'           After compacting it, this is more convenient for building a setup.
'=======================================================================================

Public Sub drop()

Const FUNCTION_NAME = "drop"

Dim tbl As ADOX.Table
Dim found As Boolean
Dim es As tSavedError

'-----------------------

On Error GoTo Catch

Do
    found = False
    For Each tbl In cat.Tables
        If StrComp(tbl.Type, "LINK", vbTextCompare) = 0 Then
            cat.Tables.Delete tbl.name
            found = True
        End If
    Next tbl
Loop While found

'-----------------------
Final:
On Error GoTo 0
RaiseSavedError es

Exit Sub

'-----------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Resume Final
Resume

End Sub
