VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsResizeForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Class to manage control resizing and repositioning with docking and centering options
'
' @author   Christoph Juengling
'
Option Explicit
#If VBA6 Then
    Option Compare Database
#End If

''
' Type to describe the docking
Private Type tDock
    Left As Boolean
    Right As Boolean
    Top As Boolean
    Bottom As Boolean
End Type

''
' Type to describe centering
Private Type tCenter
    Horizontally As Boolean
    'Vertically As Boolean
End Type

''
' Type to describe the initial position and size of a control
Private Type tPosition
    height As Long
    width As Long
    DistanceLeft As Long
    DistanceRight As Long
    DistanceTop As Long
    DistanceBottom As Long
End Type

''
' Type to describe all information of a control
Private Type tControlPositions
    name As String
    Position As tPosition
    Dock As tDock
    Center As tCenter
End Type

''
' Array to collect all control infos
Private ControlPositions() As tControlPositions

''
' Points to the highest position in the array that is currently used (-1 = nothing)
Private ControlPositionsMax As Integer

''
' Pointer to the form that is containing all controls
Private m_objForm As Form

Private Sub Class_Initialize()

ReDim ControlPositions(0 To 9)
ControlPositionsMax = -1

End Sub

Private Sub Class_Terminate()

Set m_objForm = Nothing

End Sub



Public Property Get Form() As Form

Set Form = m_objForm

End Property

Public Property Set Form(objForm As Form)

Set m_objForm = objForm

End Property


''
' Add a control to the collection
'
' @param    ctrl                The control
' @param    DockTop             Dock to top of form
' @param    DockLeft            Dock to top of form
' @param    DockRight           Dock to top of form
' @param    DockBottom          Dock to top of form
' @param    CenterHorizontally  Center control horizontally
'
Public Sub AddControl( _
    ctrl As Control, _
    Optional DockTop As Boolean = False, _
    Optional DockLeft As Boolean = False, _
    Optional DockRight As Boolean = False, _
    Optional DockBottom As Boolean = False, _
    Optional CenterHorizontally As Boolean = False _
)

If Not ctrl Is Nothing And Not m_objForm Is Nothing Then
    ControlPositionsMax = ControlPositionsMax + 1
    If UBound(ControlPositions) < ControlPositionsMax Then
        ReDim Preserve ControlPositions(0 To ControlPositionsMax + 5)
    End If
    
    ControlPositions(ControlPositionsMax).name = ctrl.name
    
    With ControlPositions(ControlPositionsMax).Position
        .height = ctrl.height
        .width = ctrl.width
        .DistanceLeft = ctrl.Left
        .DistanceTop = ctrl.Top
        .DistanceRight = m_objForm.width - ctrl.Left - ctrl.width
        .DistanceBottom = m_objForm.height - ctrl.Top - ctrl.height
    End With
    
    With ControlPositions(ControlPositionsMax).Dock
        .Top = DockTop
        .Left = DockLeft
        .Right = DockRight
        .Bottom = DockBottom
        
        If DockLeft Or DockRight Then ControlPositions(ControlPositionsMax).Center.Horizontally = False
    End With
    
    With ControlPositions(ControlPositionsMax).Center
        .Horizontally = CenterHorizontally
        If CenterHorizontally Then
            ControlPositions(ControlPositionsMax).Dock.Left = False
            ControlPositions(ControlPositionsMax).Dock.Right = False
        End If
    End With
End If

End Sub


''
' Resize form's controls
'
Public Sub PerformResize()

Const FUNCTION_NAME = "PerformResize"

Dim i As Integer
Dim Left As Long
Dim Right As Long
Dim Top As Long
Dim Bottom As Long
Dim es As tSavedError

'------------------------

On Error GoTo Catch

For i = 0 To ControlPositionsMax
    Left = -1
    Right = -1
    Top = -1
    Bottom = -1
    
    With ControlPositions(i)
        ' Prepare distances to the form's borders
        If .Dock.Left Then Left = .Position.DistanceLeft
        If .Dock.Right Then Right = .Position.DistanceRight
        If .Dock.Top Then Top = .Position.DistanceTop
        If .Dock.Bottom Then Bottom = .Position.DistanceBottom
        
        If .Center.Horizontally Then Left = Smallest((m_objForm.width - .Position.width) / 2)
        
        ' Calculate position and height/width of control
        If Left >= 0 And Right >= 0 Then
            m_objForm.Controls(.name).Left = Left
            m_objForm.Controls(.name).width = Smallest(m_objForm.width - Left - Right)
        ElseIf Left >= 0 Then
            m_objForm.Controls(.name).Left = Left
            m_objForm.Controls(.name).width = .Position.width
        ElseIf Right >= 0 Then
            m_objForm.Controls(.name).Left = Smallest(m_objForm.width - Right - .Position.width)
            m_objForm.Controls(.name).width = .Position.width
        End If
        
        If Top >= 0 And Bottom >= 0 Then
            m_objForm.Controls(.name).Top = Top
            m_objForm.Controls(.name).height = Smallest(m_objForm.height - Top - Bottom)
        ElseIf Top >= 0 Then
            m_objForm.Controls(.name).Top = Top
            m_objForm.Controls(.name).height = .Position.height
        ElseIf Bottom >= 0 Then
            m_objForm.Controls(.name).Top = Smallest(m_objForm.height - Bottom - .Position.height)
            m_objForm.Controls(.name).height = .Position.height
        End If
    End With
Next i

'------------------------
Final:
On Error Resume Next
' ... Aufräumarbeiten ...

On Error GoTo 0
RaiseSavedError es
Exit Sub

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Sub
