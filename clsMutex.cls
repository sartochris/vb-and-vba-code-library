''
' Manage an Application Mutex for the current application
'
' @remarks  Mutex name = App.Title
' @author   Christoph Juengling <christoph@juengling-edv.de>
' @link   https://bitbucket.org/juengling/vb-and-vba-code-library
'
Option Explicit

Private Declare Function CreateMutex Lib "kernel32.dll" Alias "CreateMutexA" (lpMutexAttributes As Any, ByVal bInitialOwner As Long, ByVal lpName As String) As Long
Private Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long
Private Declare Function ReleaseMutex Lib "kernel32" (ByVal hMutex As Long) As Long

Private m_lMutexHandle As Long

Private Sub Class_Initialize()

m_lMutexHandle = 0
CreateMyMutex

End Sub

Private Sub Class_Terminate()

ReleaseMyMutex

End Sub

''
' CreateMutex the Mutex
'
Public Sub CreateMyMutex()

m_lMutexHandle = CreateMutex(ByVal CLng(0), CLng(1), APPTITLE)

End Sub

''
' ReleaseMutex the Mutex and close handle
'
Public Sub ReleaseMyMutex()

If m_lMutexHandle > 0 Then
    ReleaseMutex m_lMutexHandle
    CloseHandle m_lMutexHandle
    m_lMutexHandle = 0
End If

End Sub
