''
' Split the command line
'
' @param    commandline (String)  Original command line string
' @return   String array
' @remarks  Function considers quotation marks to combine longer arguments.
' @author   Christoph Juengling
'
Public Function CommandlineSplit(Optional ByVal commandline As String = "") As String()

Dim i As Integer             ' Index
Dim char As String * 1       ' Current character to be checked
Dim cur_arg As Integer       ' Current argument's index
Dim inside_string As Boolean ' TRUE = current position is in a multi-arg string
Dim result() As String       ' Result array

'------------------------

ReDim result(0)
result(0) = ""
cur_arg = 0
inside_string = False

' Read the command line from the generic function, if it has not been specified
If commandline = "" Then commandline = Command()

For i = 1 To Len(commandline)
    char = Mid(commandline, i, 1)
    
    Select Case char
        Case """"
            inside_string = Not inside_string
            
        Case " "
            If inside_string Then
                ' We are inside of a multi-arg string: Space is a normal character
                result(cur_arg) = result(cur_arg) & char
            Else
                ' We are outside of a multi-arg string: Space is a delimiter
                cur_arg = cur_arg + 1
                ReDim Preserve result(cur_arg)
            End If
        
        Case Else
            result(cur_arg) = result(cur_arg) & char
    End Select
Next i

CommandlineSplit = result

End Function
