VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCheckVersions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'=======================================================================================
' Module  : clsCheckVersion (Klassenmodul)
' Purpose : Check version of THIS application against the setup.exe file
' Author  : Dev Ashish
' Author  : Class made by Christoph J�ngling <Christoph@Juengling-EDV.de>
' Throws  : NewVersionAvailable
' Imports :
' Remarks : This code was originally written by Dev Ashish.
'           It is not to be altered or distributed,
'           except as part of an application.
'           You are free to use it in any application,
'           provided the copyright notice is left unchanged.
'           Code Courtesy of Dev Ashish
'=======================================================================================

Option Explicit

Private p_strFileVersionNumber As String
Private p_strFilePath As String
Private p_bolNewerVersionAvailable As Boolean

''
'  structure contains version information about a file. This
'  information is language and code page independent.
Private Type VS_FIXEDFILEINFO
    ''
    ' Contains the value 0xFEEFO4BD (szKey)
    dwSignature As Long
    
    ''
    ' Specifies the binary version number of this structure.
    dwStrucVersion As Long
    
    ''
    ' most significant 32 bits of the file's binary version number.
    dwFileVersionMS As Long
    
    ''
    ' least significant 32 bits of the file's binary version number.
    dwFileVersionLS As Long
    
    ''
    ' most significant 32 bits of the binary version number of
    ' the product with which this file was distributed
    dwProductVersionLS As Long
    
    ''
    ' least significant 32 bits of the binary version number of
    ' the product with which this file was distributed
    dwFileFlagsMask As Long
    
    ''
    '  Contains a bitmask that specifies the valid bits in dwFileFlags.
    dwProductVersionMS As Long
    
    ''
    ' Contains a bitmask that specifies the Boolean attributes of the file.
    dwFileFlags As Long
    
    ''
    ' operating system for which this file was designed.
    dwFileOS As Long
    
    ''
    ' general type of file.
    dwFileType As Long
    
    ''
    '  function of the file.
    dwFileSubtype As Long
    
    ''
    ' most significant 32 bits of the file's 64-bit binary creation date and time stamp.
    dwFileDateMS As Long
    
    ''
    ' least significant 32 bits of the file's 64-bit binary creation date and time stamp.
    dwFileDateLS As Long
End Type

'  Returns size of version info in Bytes
Private Declare Function apiGetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long

'  Read version info into buffer
' /* Length of buffer for info *
' /* Information from GetFileVersionSize *
' /* Filename of version stamped file *
Private Declare Function apiGetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, lpData As Any) As Long

'  returns selected version information from the specified
'  version-information resource.
Private Declare Function apiVerQueryValue Lib "version.dll" Alias "VerQueryValueA" (pBlock As Any, ByVal lpSubBlock As String, lplpBuffer As Long, puLen As Long) As Long

Private Declare Sub sapiCopyMem Lib "kernel32" Alias "RtlMoveMemory" (destination As Any, source As Any, ByVal length As Long)

'=======================================================================================
' Purpose : Returns the build number for Applications
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
'=======================================================================================

Private Sub getProductVersion()

Const FUNCTION_NAME = "GetProductVersion"

Dim es As tSavedError
Dim lngSize As Long
Dim lngRet As Long
Dim pBlock() As Byte
Dim lpfi As VS_FIXEDFILEINFO
Dim lppBlock As Long
Dim versionString() As String

'-----------------------

On Error GoTo Catch

'  GetFileVersionInfo requires us to get the size
'  of the file version information first, this info is in the format
'  of VS_FIXEDFILEINFO struct
lngSize = apiGetFileVersionInfoSize(p_strFilePath, lngRet)

'  If the OS can obtain version info, then proceed on
If lngSize Then
    '  the info in pBlock is always in Unicode format
    ReDim pBlock(lngSize)
    lngRet = apiGetFileVersionInfo(p_strFilePath, 0, lngSize, pBlock(0))
    If Not lngRet = 0 Then
        '  the same pointer to pBlock can be passed to VerQueryValue
        lngRet = apiVerQueryValue(pBlock(0), "\", lppBlock, lngSize)

        '  fill the VS_FIXEDFILEINFO struct with bytes from pBlock
        '  VerQueryValue fills lngSize with the length of the block.
        sapiCopyMem lpfi, ByVal lppBlock, lngSize
        '  build the version info strings
        With lpfi
            p_strFileVersionNumber = hiWord(.dwFileVersionMS) & "." _
                & loWord(.dwFileVersionMS) & "." _
                & hiWord(.dwFileVersionLS) & "." _
                & loWord(.dwFileVersionLS)
        End With
    End If
End If

'-----------------------
Final:
Erase pBlock

On Error GoTo 0
RaiseSavedError es

Exit Sub

'-----------------------
Catch:
es = SaveError(typeName(Me) & "." & FUNCTION_NAME, Erl)
Resume Final
Resume

End Sub

'=======================================================================================
' Purpose : retrieves the low-order word from the given 32-bit value
' Author  : Dev Ashish
'=======================================================================================

Private Function loWord(ByVal dw As Long) As Integer

If dw And &H8000& Then
    loWord = dw Or &HFFFF0000
Else
    loWord = dw And &HFFFF&
End If

End Function

'=======================================================================================
' Purpose : retrieves the high-order word from the given 32-bit value
' Author  : Dev Ashish
'=======================================================================================

Private Function hiWord(ByVal dw As Long) As Integer

hiWord = (dw And &HFFFF0000) \ &H10000

End Function

Public Property Get fileVersionNumber() As String

fileVersionNumber = p_strFileVersionNumber

End Property

Public Property Get filePath() As String

filePath = p_strFilePath

End Property

Public Property Let filePath(ByVal strFilePath As String)

p_strFilePath = strFilePath
getProductVersion
'compareVersions

End Property






