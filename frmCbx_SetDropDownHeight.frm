VERSION 5.00
Begin VB.Form frmCbx_SetDropDownHeight 
   Caption         =   "frmCbx_SetDropDownHeight"
   ClientHeight    =   3960
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   6810
   LinkTopic       =   "Form1"
   ScaleHeight     =   3960
   ScaleWidth      =   6810
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox Cbx 
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      Text            =   "Cbx"
      Top             =   720
      Width           =   2775
   End
End
Attribute VB_Name = "frmCbx_SetDropDownHeight"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function MoveWindow Lib "user32" ( _
  ByVal hwnd As Long, _
  ByVal x As Long, _
  ByVal Y As Long, _
  ByVal nWidth As Long, _
  ByVal nHeight As Long, _
  ByVal bRepaint As Long) As Long
  
' Aufklapphöhe einer ComboBox neu festlegen
' (Angabe in Pixel)
Private Sub Cbx_SetDropDownHeight_(F As Form, _
  oCbx As ComboBox, ByVal nHeight As Long)
 
  With oCbx
    MoveWindow .hwnd, _
      F.ScaleX(.Left, .Parent.ScaleMode, vbPixels), _
      F.ScaleY(.Top, .Parent.ScaleMode, vbPixels), _
      F.ScaleX(.Width, .Parent.ScaleMode, vbPixels), _
      nHeight, 1
  End With
End Sub

Private Sub Form_Load()

  ' Kombinationsfeld mit Daten füllen
  Dim i As Integer
  
  For i = 0 To 20
    Me.Cbx.AddItem "Eintrag " & CStr(i)
  Next i
 
  ' Aufklapphöhe neu festlegen
  Cbx_SetDropDownHeight_ Me, Me.Cbx, 300
End Sub
