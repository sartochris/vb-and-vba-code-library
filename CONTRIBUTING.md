To contribute to this library (thanks in advance!) I recommend to

* **fork** the repository
* make some changes (add new code, change existing, or fix an error)
* create a **merge request**
