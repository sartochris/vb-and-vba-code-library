''
' Example initialize function to be used in AutoExec macro
'
' @author   Christoph Juengling <chris@juengling-edv.de>
' @see Terminate
'
Public Function Initialize()

Const FUNCTION_NAME = "Initialize"

Dim pb As clsProgressControl
Dim es As tSavedError

'-----------------------

On Error GoTo Catch

Set pb = ProgressControl

pb.displaytext = "Initialize"
pb.addsteps 2

pb.extendeddisplaytext = "Attach tables"
AttachTables
pb.step

DoCmd.OpenForm "Main"
pb.step

pb.extendeddisplaytext = ""
pb.displaytext = "Beam me up, Scotty!"
wait 3
pb.hide

'-----------------------
Final:
On Error Resume Next
Set pb = Nothing

On Error GoTo 0
ShowSavedError es

Exit Function

'-----------------------
Catch:
es = SaveError(CLASS_NAME, FUNCTION_NAME, Erl)

Select Case es.Number
    Case Else
        Debug.Print es.Number, es.source, es.Description
End Select

Resume Final
Resume

End Function

''
' Example terminate function
'
' @author   Christoph Juengling <chris@juengling-edv.de>
' @see Initialize
'
Public Sub Terminate()

Const FUNCTION_NAME = "Terminate"

Dim es As tSavedError

'-----------------------

On Error GoTo Catch

' TODO: Close any form except "Main"
' TODO: Terminate any instance, especially Singletons!
' TODO: Clean any temporary folder solely created and used by this application

' At least show user we are done
DoCmd.Close acForm, "Main"

'-----------------------
Final:
On Error GoTo 0
RaiseSavedError es

Exit Sub

'-----------------------
Catch:
es = SaveError(CLASS_NAME, FUNCTION_NAME, Erl)

Select Case es.Number
    Case Else
        Debug.Print es.Number, es.source, es.Description
End Select

Resume Final
Resume

End Sub