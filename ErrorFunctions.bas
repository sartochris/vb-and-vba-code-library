Attribute VB_Name = "ErrorFunctions"
''
' Declarations and functions for convenient exception (VB: error) handling
'
' @author Christoph Juengling <christoph@juengling-edv.de>
' @link   https://bitbucket.org/juengling/vb-and-vba-code-library
'
Option Explicit

''
' Enumeration of internal error numbers
' Add necessary constants to this enumeration
' I have prepared some usual values for your convenience
'
' @param ERR_INTERNAL               Internal error (used for several messages)
' @param ERR_ILLEGAL_VALUE          Illegal value
' @param ERR_VALUE_OUT_OF_RANGE     Value out of allowed range
' @param ERR_WRONG_PATH             Wrong path
' @param ERR_ABORTED                Any process has been aborted
' @param ERR_NO_RECORD_FOUND        No record found
' @param ERR_NO_DEFAULT_PRINTER     No default printer
' @param ERR_REGEX                  Error in regular expression
' @param ERR_DUPLICATE_NAME         Duplicate name
' @param ERR_DB_OPEN_ERROR          Error when opening a database
' @param ERR_MASK_UPDATE            Error in mask update procedure
' @param ERR_PARSE                  XML parse error
' @param ERR_DB_ALREADY_OPEN        Database is already open
' @param Count                      Returns the number of the enum entries. This entry must be the last one in this enum!
'
Public Enum eProjectErrorNumbers
    ERR_INTERNAL
    ERR_ILLEGAL_VALUE
    ERR_VALUE_OUT_OF_RANGE
    ERR_CONFIG_ERROR
    ERR_WRONG_PATH
    ERR_ABORTED
    ERR_NO_RECORD_FOUND
    ERR_NO_DEFAULT_PRINTER
    ERR_EXPORT
    ERR_REGEX
    ERR_DUPLICATE_NAME
    ERR_DB_OPEN_ERROR
    ERR_PARSE
    ERR_DB_ALREADY_OPEN
    
    ' This entry must always be the last one in this enum!
    Count
End Enum

''
' Type declaration to save any error
'
' @param Description    Error description
' @param Source         Source class where the error was raised
' @param Number         Error number
' @param Helpfile       Helpfile reference
' @param HelpContext    Helpcontext reference
' @param MsgBoxOnly     Show only message box (i.e. without further error information)
'
Public Type tSavedError
    Description As String
    Source As String
    Number As Long
    Helpfile As String
    HelpContext As String
    MsgBoxOnly As Boolean
End Type

''
' Return the VB error number for the given project error number
'
' @param    ProjectErrorNumber
' @return   VB error number

Public Function GetVBErrorNumber(ByVal ProjectErrorNumber As eProjectErrorNumbers) As Long

GetVBErrorNumber = vbObjectError + 513 + ProjectErrorNumber

End Function

''
' Return the project error number for the given VB error number
'
' @param    VBErrorNumber
' @return   project error number

Public Function GetProjectErrorNumber(ByVal VBErrorNumber As Long) As eProjectErrorNumbers

GetProjectErrorNumber = VBErrorNumber - vbObjectError - 513

End Function

''
' Raise project error by given project error number
'
' @param    e               Project error number
' @param    ClassName       Name of the class or form where the error raised
' @param    FunctionName    Name of the sub or function where the error raised
' @param    ErrorMessage    Line number where the error raised

Public Sub RaiseProjectError( _
    ByVal e As eProjectErrorNumbers, _
    Optional ByVal ClassName As String = "", _
    Optional ByVal FunctionName As String = "", _
    Optional ByVal ErrorMessage As String = "" _
)

Dim strSource As String

If ClassName = "" Or FunctionName = "" Then
    strSource = ClassName & FunctionName
Else
    strSource = ClassName & "." & FunctionName
End If

err.Raise GetVBErrorNumber(e), strSource, ErrorMessage

End Sub

''
' Raise error from saved structure
'
' @param    save   Saved error informations
' @remarks  If error number = 0, no error is raised

Public Sub RaiseSavedError(save As tSavedError)

With save
    If .Number <> 0 Then err.Raise .Number, .Source, .Description, .Helpfile, .HelpContext
End With

End Sub

''
' Store the current error into a structure
'
' @param    ClassName       Name of the class or form where the error raised
' @param    FunctionName    Name of the sub or function where the error raised
' @param    ErrorLine       Line number where the error raised
' @return   Structure of type tSavedError with error informations

Public Function SaveError(Optional ByVal ClassName As String = "", Optional ByVal FunctionName As String = "", Optional ByVal ErrorLine As Long = 0) As tSavedError

Dim save As tSavedError
Dim NewSource As String

'---------------------
 
save.Number = err.Number
save.Helpfile = err.Helpfile
save.HelpContext = err.HelpContext
save.Description = err.Description
save.MsgBoxOnly = False

If ClassName <> "" And FunctionName <> "" Then
    NewSource = ClassName & "." & FunctionName
Else
    NewSource = ClassName & FunctionName
End If

If ErrorLine > 0 Then NewSource = NewSource & " (" & ErrorLine & ")"

If err.Source <> "" And NewSource <> "" Then
    save.Source = NewSource & " / " & err.Source
ElseIf save.Source <> "" Then
    save.Source = err.Source
Else
    save.Source = NewSource
End If

SaveError = save

err.Clear

End Function

''
' Display a message box with a saved error
'
' @param    save   Saved error informations
' @remarks  If error number = 0, no error will be raised

Public Sub ShowSavedError(save As tSavedError)

Dim msg As String
Dim mainFileName As String
Dim strFullVersionNumber As String

'----------------------

#If VBA5 Or VBA6 Or VBA7 Then
    mainFileName = CurrentDb.Properties("AppTitle") ' Access only
#Else
    mainFileName = App.exeName & ".exe" ' VB6 only
#End If

With save
    If .Number <> 0 Then
        If (.MsgBoxOnly Or IsProjectError(.Number)) Then
            msg = Trim$(.Description)
            MsgBox msg, vbExclamation + vbApplicationModal, "Error"
        Else
            msg = Trim$(.Description) & vbNewLine & vbNewLine
            msg = msg & "Application: " & mainFileName & strFullVersionNumber & vbNewLine & vbNewLine
            msg = msg & "Function: " & .Source & vbNewLine & vbNewLine
            MsgBox msg, vbCritical + vbApplicationModal, "Error " & .Number
        End If
    End If
End With

End Sub



''
' Display a message box with a saved error only without any code details
'
' @param    save   Saved error informations
' @remarks  If error number = 0, no error is raised

Public Sub ShowSavedErrorMinimal(save As tSavedError)

If save.Number <> 0 Then
    MsgBox Trim$(save.Description), vbCritical + vbApplicationModal, "Error"
End If

End Sub






''
' Checks if given error is a project error
'
' @param    VBErrorNumber   VB error number
' @return   TRUE if error number is in project error range
'
Public Function IsProjectError(VBErrorNumber As Long) As Boolean

IsProjectError = (VBErrorNumber >= GetVBErrorNumber(0) And VBErrorNumber <= GetVBErrorNumber(eProjectErrorNumbers.Count))

End Function
