Attribute VB_Name = "basCbxSetListWidth"
Option Explicit

Private Const CB_SETDROPPEDWIDTH = &H160

Public Declare Function SendMessage Lib "User32" _
  Alias "SendMessageA" ( _
  ByVal hwnd As Long, _
  ByVal wMsg As Long, _
  ByVal wParam As Long, _
  ByVal lParam As Long) As Long
 

'' ListBox-Breite auf 2000 Twips festlegen
'SendMessage Cbx.hwnd, CB_SETDROPPEDWIDTH, (2000 / Screen.TwipsPerPixelX), 0&

' Aufklappbreite der ComboBox-Liste fix oder
' autom. anhand des l�ngsten Eintrag festlegen
Private Sub CbxSetListWidth_(oCbx As Object, _
  Optional ByVal nFixWidth As Variant, _
  Optional ByVal nScaleMode As Variant)
 
  With oCbx
    ' Falls keine Ma�einheit angegeben,
    ' autom. die ScaleMode-Eigenschaft der Form
    ' verwenden
    If IsMissing(nScaleMode) Or IsMissing(nFixWidth) Then
      nScaleMode = .Parent.ScaleMode
    End If
 
    ' Falls Parameter "nFixWidth" nicht angegeben,
    ' l�ngsten Eintrag ermitteln
    If IsMissing(nFixWidth) Then
      Dim i As Long
      Dim nWidth As Long
 
      nFixWidth = 0
      For i = 0 To .ListCount - 1
        nWidth = .Parent.TextWidth(.List(i))
        If nWidth > nFixWidth Then nFixWidth = nWidth
      Next i
 
      ' Plus kleine Zugabe
      nFixWidth = nFixWidth + .Parent.ScaleX(10, vbPixels, nScaleMode)
 
      ' Falls mehr als 8 Eintr�ge, nochmals kleine Zugabe wegen der Scrollbar
      If .ListCount > 8 Then
        nFixWidth = nFixWidth + .Parent.ScaleX(15, vbPixels, nScaleMode)
      End If
    End If
 
    ' Neue Aufklappbreite jetzt zuweisen
    SendMessage .hwnd, CB_SETDROPPEDWIDTH, _
      .Parent.ScaleX(nFixWidth, nScaleMode, vbPixels), 0&
  End With
End Sub


'// Aufruf der Routine CbxSetListWidth_()
Private Sub Form_Load()

'    ' Breite der ListBox autom. anpassen
'    ComboSetListWidth Me.Cbx
    
    ' Breite der ListBox fest auf 250 Pixel festlegen
    CbxSetListWidth_ Me.Cbx, 250, vbPixels
End Sub



