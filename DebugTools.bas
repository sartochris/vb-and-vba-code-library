''
' Debug Tools
'
' @author   Christoph Juengling
'
Option Explicit
' Option Compare Database ' Access only

''
' Display name and caption of open forms in debug window
'
' @author   Christoph Juengling
'
Public Sub f()

Dim i As Integer

Debug.Print Forms.count & " forms open at " & Now
For i = 0 To Forms.count - 1
    Debug.Print i; Tab(5); Forms(i).name; Tab(30); Forms(i).caption
Next i

End Sub
