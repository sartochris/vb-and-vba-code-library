''
' Simple concatenation function for paths
'
' @author  Christoph Juengling <chris@juengling-edv.de>
' @returns Concatenated path
' @comment Function does not check, if this would result in a correct path!

Public Function BuildPath(ParamArray Parts()) As String

Const FUNCTION_NAME = "BuildPath"

Dim es As tSavedError
Dim strResult As String
Dim i As Long

'------------------------

On Error GoTo Catch

BuildPath = ""
strResult = ""
For i = LBound(Parts) To UBound(Parts)
    If strResult = "" Then
        strResult = Parts(i)
    Else
        If Right(strResult, 1) <> "\" Then strResult = strResult & "\"
        strResult = strResult & Parts(i)
    End If
Next i

BuildPath = strResult

'------------------------
Final:
On Error GoTo 0
RaiseSavedError es
Exit Function

'------------------------
Catch:
es = SaveError(CLASS_NAME, FUNCTION_NAME, Erl)
Select Case es.Number
    Case Else
        Debug.Print es.Number, es.source, es.Description
End Select
Resume Final
Resume ' for test purposes only

End Function